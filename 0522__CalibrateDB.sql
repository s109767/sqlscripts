DROP DATABASE IF EXISTS ModernWays;
CREATE DATABASE IF NOT EXISTS `ModernWays` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ModernWays`;

LOCK TABLES `Huisdieren` WRITE;
/*!40000 ALTER TABLE `Huisdieren` DISABLE KEYS */;
INSERT INTO `Huisdieren` VALUES
('Misty',6,'hond','Vincent','WAF!'),
('Ming',9,'hond','Christiane','WAF!'),
('Bientje',6,'kat','Esther','miauwww...'),
('Ming',9,'kat','Bert','miauwww...'),
('Suerta',2,'hond','Thaïs','WAF!'),
('Aran',6,'hond','Thaïs','WAF!'),
('Mojo',12,'hond','Thaïs','WAF!'),
('Bollie',14,'kat','Truus','miauwww...'),
('Фёдор',1,'hond','Lyssa','WAF!');
/*!40000 ALTER TABLE `Huisdieren` ENABLE KEYS */;
UNLOCK TABLES;
